# README

## Signpost Icons
- Exposes an SVG icon field, formatter, and widget to allow admins to choose from a set of pre-defined SVG fields.

## Usage
- After enabling this module, add {{ svg_symbols }} into your html.html.twig immediately following <body> element

### Developer info
- `svg/src/` contains all the SVG source files.
- Running gulpfile.js will generate `twig/svg-symbols.svg.twig`, which will be included as the first HTML element in all pageloads where this widget gets rendered.
- The default symbols file can be overridden by custom theme implementation, see "Extending"

### Extending
- To implement new site-specific SVG sources, or to override existing ones:
  - Create a directory in your theme named like `svg/src` and put your SVGs in it.
  - Edit the gulp file and add your new directory as a source
  - re-gulp to generate the include file
- To add a new SVG icon to the upstream, drop it into svg/src
  - SVG icon sources are assumed to contain at most one symbol per document