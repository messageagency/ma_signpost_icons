<?php

namespace Drupal\ma_signpost_icons\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'signpost_icon' formatter.
 *
 * @FieldFormatter(
 *   id = "signpost_icon",
 *   module = "signpost_icon",
 *   label = @Translation("Signpost Icon"),
 *   field_types = {
 *     "signpost_icon"
 *   }
 * )
 */
class SignpostIcon extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#theme' => 'svg_use',
        '#icon' => ma_signpost_get_svg_url() . $item->value,
      );
    }

    return $elements;
  }

}
