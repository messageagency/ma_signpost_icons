<?php

namespace Drupal\ma_signpost_icons\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'signpost_icon' widget.
 *
 * @FieldWidget(
 *   id = "signpost_icon",
 *   module = "signpost_icon",
 *   label = @Translation("Signpost Icon"),
 *   field_types = {
 *     "signpost_icon"
 *   }
 * )
 */
class SignpostIcon extends WidgetBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'select',
      '#title' => $this->t('Signpost Icon'),
      '#description' => $this->t('Select Icon.'),
      '#default_value' => $value,
      '#options' => $this->getSvgSelectOptions(),
    ];
    return ['value' => $element];

  }

  private function getSvgSelectOptions() {
    $options = ['' => '(none)'];
    $files = ma_signpost_icons_get_files(FALSE);
    foreach ($files as $fileinfo) {
      $options['#' . strtolower($fileinfo->name)] = t('%icon_name icon', ['%icon_name' => $fileinfo->name]);
    }
    return $options;
  }

}
