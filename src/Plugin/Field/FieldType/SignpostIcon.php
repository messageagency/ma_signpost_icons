<?php

namespace Drupal\ma_signpost_icons\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'signpost_icon' field type.
 *
 * @FieldType(
 *   id = "signpost_icon",
 *   label = @Translation("Signpost Icon"),
 *   module = "signpost_icon",
 *   description = @Translation("An SVG icon."),
 *   default_widget = "signpost_icon",
 *   default_formatter = "signpost_icon"
 * )
 */
class SignpostIcon extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'small',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('SVG symbol ID'));

    return $properties;
  }

}
